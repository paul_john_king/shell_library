#	The function call
#
#		is_a_natural_number «string»
#
#	returns 0 if the string «string» is a representation of a natural number,
#	returns 1 if the string «string» is not a representation of a natural
#	number, and prints a message and returns 2 if a failure occurs.	

#	This library has no dependencies.

is_a_natural_number()
{

	if test ${#} -lt 1 ;
	then

		#	I have less than 1 arguments.  I print a message and return 2.

		echo "Function 'is_a_natural_number' requires at least 1 argument." ;

		return 2 ;

	fi ;

	case "${1}" in

		''|*[!0-9]*)

			#	My first argument is the empty string "" or a string that
			#	contains a character other than a digit.  My first argument is
			#	therefore not a representation of a natural number.  I return
			#	1.

			return 1 ;

		;;

		*)

			#	My first argument is a string that contains only digits.  My
			#	first argument is therefore a representation of a natural
			#	number.  I return 0.

			return 0 ;

		;;

	esac ;

}
