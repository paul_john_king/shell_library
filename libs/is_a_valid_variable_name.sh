#	The function call
#
#		is_a_valid_variable_name «string»
#
#	returns 0 if the string «string» is a valid variable name, returns 1 if the
#	string «string» is not a valid variable name, and prints a message and
#	returns 2 if a failure occurs.

#	This library has no dependencies.

is_a_valid_variable_name()
{

	if test ${#} -lt 1 ;
	then

		#	I have less than 1 arguments.  I print a message and return 2.

		echo "Function 'is_a_valid_variable_name' requires at least 1 argument." ;

		return 2 ;

	fi ;

	case "${1}" in

		'')

			#	My first argument is the empty string.  My first argument is
			#	therefore not a valid variable name.  I return 1.

			return 1 ;

		;;

		*[!0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz_]*)

			#	Some character in my first argument is neither an ASCII
			#	alphanumeric character nor the underscore character "_".  My
			#	first argument is therefore not a valid variable name.  I
			#	return 1.

			return 1 ;

		;;

		[!ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz_]*)

			#	The first character in my first argument is neither an ASCII
			#	alphabetic character nor the underscore character "_".  My
			#	first argument is therefore not a valid variable name.  I
			#	return 1.

			return 1 ;

		;;

		*)

			#	Each character in my first argument is an ASCII alphanumeric
			#	character or the underscore character "_", and the first
			#	character in my first argument is not a number.  My first
			#	argument is therefore a valid variable name.  I return 0.

			return 0 ;

		;;

	esac ;

}

