#	The function call
#
#		identify_shell
#
#	prints the name and version of the shell it is called in if known, or
#	prints 'unknown' if not, and returns 0.  The function currently only
#	recognises the bash shell, some Korn shells, and the zsh shell.

#	This library has the following dependencies:
#
#		names_a_set_variable

identify_shell()
{

	#	I try to identify (by sequentially testing whether certain variables
	#	are set) and print the name and version of the shell in which I am
	#	called.

	if names_a_set_variable "BASH_VERSION" ;
	then

		echo "bash ${BASH_VERSION}" ;

	elif names_a_set_variable "KSH_VERSION" ;
	then

		echo "ksh ${KSH_VERSION}" ;

	elif names_a_set_variable "ZSH_VERSION" ;
	then

		echo "zsh ${ZSH_VERSION}" ;

	else

		echo "unknown" ;

	fi ;

	return 0 ;

}
