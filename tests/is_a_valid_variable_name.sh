set -u ;

if ! test "${PROJECT_HOME+x}" ;
then

	echo "PROJECT_HOME is not set." >&2 ;
	exit 1 ;

fi ;

. "${PROJECT_HOME}/libs/is_a_valid_variable_name.sh" ;

test_is_a_valid_variable_name()
{

	#	I test how the function responds to an incorrect number of arguments.

	rstring=$( is_a_valid_variable_name ) ;
	test ${?} -eq 2 -a \
	     "${rstring}" = "Function 'is_a_valid_variable_name' requires at least 1 argument." ;
	assertTrue "The function responds to 0 arguments with a message and status 2" ${?} ;

	#	I test how the function responds to an invalid variable name.

	for tstring in "8foo" "8foo8" "8foo8bar" "8_foo" "8_foo8" "8_foo8bar" \
	               "8FOO" "8FOO8" "8FOO8BAR" "8_FOO" "8_FOO8" "8_FOO8BAR" \
	               "8FoO" "8fOo8" "8fOo8bAr" "8_fOo" "8_fOo8" "8_FoO8BaR" \
	               "" \
	               "nonツascii" \
	               "shell !$&*@#" \
	               "-leading hyphen" \
	               "single'quote" 'double"quote' \
	               "white space" "white  spaces" "  leading spaces" "trailing spaces  " \
	               "tab	space" "tab		spaces" "		leading tabs" "trailing tabs		" \
	               "new
line" ;
	do

		rstring=$( is_a_valid_variable_name "${tstring}" ) ;
		test ${?} -eq 1 -a "${rstring}" = "" ;
		assertTrue "The function responds to invalid variable name '${tstring}' with no message and status 1" ${?} ;

	done ;

	#	I test how the function responds to a valid variable name.

	for tstring in "foo" "foo8" "foo8bar" "_foo" "_foo8" "_foo8bar" \
	               "FOO" "FOO8" "FOO8BAR" "_FOO" "_FOO8" "_FOO8BAR" \
	               "FoO" "fOo8" "fOo8bAr" "_fOo" "_fOo8" "_FoO8BaR" ;
	do

		rstring=$( is_a_valid_variable_name "${tstring}" ) ;
		test ${?} -eq 0 -a "${rstring}" = "" ;
		assertTrue "The function responds to valid variable name '${tstring}' with no message and status 0" ${?} ;

	done ;

	unset tstring ;
	unset rstring ;

}
