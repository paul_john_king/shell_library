#	The function call
#
#		solicit_choice «string» «string_1» ... «string_n»
#
#	repeatedly prints the string «string» followed by the enumerated list
#
#		1)   «string_1»
#		...
#		n)   «string_n»
#
#	of strings and asks the user to enter
#
#		a number i from 1 to n inclusive in order to choose the string
#		«string_i», or
#
#		0 to make no choice
#
#	until the user inputs a number from 0 to n inclusive, and returns i; and
#	prints a message and returns 255 if n is larger than 254 or a failure
#	occurs.

#	This function has the following dependencies:
#
#		is_a_natural_number

solicit_choice()
{

	if test ${#} -lt 2 -o ${#} -gt 255 ;
	then

		#	I have less than 2 or more than 255 arguments.  I print a message
		#	and return 255.

		echo "Function 'solicit_choice' requires from 2 to 255 arguments." ;

		return 255 ;

	fi ;

	#	In a subshell (in order to localise variables - see note "Global and
	#	Local Variables" in 'showcase.sh'),...

	(

		#	I save my first argument as a message and shift it off of my
		#	argument stack.

		message="${1}" ;
		shift ;

		#	In an infinite loop,...

		while true ;
		do

			#	I print the message.

			echo "${message}" ;
			echo "" ;

			#	I zero an index.

			index=0 ;

			#	For each string in my argument stack,...

			for string in "${@}" ;
			do

				#	I increment the index, and pretty print the index and
				#	string.

				index=$(( index + 1 )) ;

				printf "    %-4s %s\n" "${index})" "${string}" ;

			done ;

			#	I ask the user to choose an index or 0 and I read their choice.

			echo "" ;
			echo -n "Enter the number of an option to choose that option, or 0 to make no choice. " ;

			IFS= read -r choice ;

			#	I test the user's input.

			if is_a_natural_number "${choice}" && test ${choice} -le ${index} ;
			then

				#	The user made a valid choice.  I break the infinite loop.

				break ;

			else

				#	The user made an invalid choice.  I print a message and
				#	continue the infinite loop

				echo "Sorry, '${choice}' is not a valid number." ;

				continue ;

			fi ;

		done ;

		#	I return the user's choice.

		return ${choice} ;

	) ;

}
