#	The function call
#
#		print_choice «i» «string_1» ... «string_n»
#
#	prints the string «string_i» and returns 0 if «i» is a natural number in
#	the range 1 to n inclusive, returns 1 if «i» is not a natural number in the
#	range 1 to n inclusive, and prints a message and returns 2 if a failure
#	occurs.

#	This library has the following dependencies:
#
#		is_a_natural_number

print_choice()
{

	if test ${#} -lt 1 ;
	then

		#	I have less than 1 arguments.  I print a message and return 2.

		echo "Function 'print_choice' requires at least 1 argument." ;

		return 2 ;

	fi ;

	if ! is_a_natural_number "${1}" ;
	then

		#	My first argument is not a natural number.  I return 1.

		return 1 ;

	fi ;

	if test ${1} -lt 1 -o ${1} -ge ${#} ;
	then

		#	My first argument is not in the correct range.  I return 1.

		return 1 ;

	fi ;

	#	My first argument is a natural number «i» in the correct range.  I
	#	shift «i» arguments off of my argument stack.  My then current first
	#	argument is the «i»th string.  I print this string and return 0.

	shift ${1} ;

	echo "${1}" ;

	return 0 ;

}
