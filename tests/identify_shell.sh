set -u ;

if ! test "${PROJECT_HOME+x}" ;
then

	echo "PROJECT_HOME is not set." >&2 ;
	exit 1 ;

fi ;

. "${PROJECT_HOME}/libs/is_a_valid_variable_name.sh" ;
. "${PROJECT_HOME}/libs/names_a_set_variable.sh" ;
. "${PROJECT_HOME}/libs/identify_shell.sh" ;

#	I cannot unit test the 'identify_shell' function in every shell.  The
#	function determines the type and version of a shell by examining different
#	variables that different shells set.  However, some shells set these
#	variables read only, and so I cannot reliably manipulate these variables in
#	order to test the function in these shells.

test_identify_shell()
{

	#	For each shell version I examine,...

	for shell_version in "BASH_VERSION" "KSH_VERSION" "ZSH_VERSION" ;
	do

		#	I try to unset the shell version.

		if ! unset "${shell_version}" 2>/dev/null ;
		then

			#	I cannot unset the shell version, and therefore cannot unit
			#	test the function in this shell.  I print a message and return
			#	0.

			echo "I cannot test this function in this shell."

			return 0 ;

		fi ;

	done ;

	#	I test how the function responds when no shell version is set.

	rstring=$( identify_shell ) ;
	test ${?} -eq 0 -a "${rstring}" = "unknown" ;
	assertTrue "The function identifies the unknown shell and returns 0" ${?} ;

	#	I test how the function responds when the bash version is set.

	BASH_VERSION="bash_version" ;

	rstring=$( identify_shell ) ;
	test ${?} -eq 0 -a "${rstring}" = "bash bash_version" ;
	assertTrue "The function identifies the bash shell and returns 0" ${?} ;

	unset BASH_VERSION ;

	#	I test how the function responds when the ksh version is set.

	KSH_VERSION="ksh_version" ;

	rstring=$( identify_shell ) ;
	test ${?} -eq 0 -a "${rstring}" = "ksh ksh_version" ;
	assertTrue "The function identifies the ksh shell and returns 0" ${?} ;

	unset KSH_VERSION ;

	#	I test how the function responds when the zsh version is set.

	ZSH_VERSION="zsh_version" ;

	rstring=$( identify_shell ) ;
	test ${?} -eq 0 -a "${rstring}" = "zsh zsh_version" ;
	assertTrue "The function identifies the zsh shell and returns 0" ${?} ;

	unset ZSH_VERSION ;

}
