set -u ;

if ! test "${PROJECT_HOME+x}" ;
then

	echo "PROJECT_HOME is not set." >&2 ;
	exit 1 ;

fi ;

. "${PROJECT_HOME}/libs/is_a_natural_number.sh" ;
. "${PROJECT_HOME}/libs/solicit_choice.sh" ;

test_solicit_choice()
{

	#	I test how the function responds to various argument counts.

	rstring=$( solicit_choice ) ;
	test ${?} -eq 255 -a \
	     "${rstring}" = "Function 'solicit_choice' requires from 2 to 255 arguments." ;
	assertTrue "The function responds to 0 arguments with a message and status 255" ${?} ;

	rstring=$( solicit_choice 1 ) ;
	test ${?} -eq 255 -a \
	     "${rstring}" = "Function 'solicit_choice' requires from 2 to 255 arguments." ;
	assertTrue "The function responds to 1 argument with a message and status 255" ${?} ;

	rstring=$( echo 0 | solicit_choice 1 2 ) ;
	test ${?} -eq 0 ;
	assertTrue "The function responds to 2 arguments and choice 0 with status 0" ${?} ;

	rstring=$( echo 0 | solicit_choice 001 002 003 004 005 006 007 008 009 010 011 012 013 014 015 016 017 018 019 020 021 022 023 024 025 026 027 028 029 030 031 032 033 034 035 036 037 038 039 040 041 042 043 044 045 046 047 048 049 050 051 052 053 054 055 056 057 058 059 060 061 062 063 064 065 066 067 068 069 070 071 072 073 074 075 076 077 078 079 080 081 082 083 084 085 086 087 088 089 090 091 092 093 094 095 096 097 098 099 100 101 102 103 104 105 106 107 108 109 110 111 112 113 114 115 116 117 118 119 120 121 122 123 124 125 126 127 128 129 130 131 132 133 134 135 136 137 138 139 140 141 142 143 144 145 146 147 148 149 150 151 152 153 154 155 156 157 158 159 160 161 162 163 164 165 166 167 168 169 170 171 172 173 174 175 176 177 178 179 180 181 182 183 184 185 186 187 188 189 190 191 192 193 194 195 196 197 198 199 200 201 202 203 204 205 206 207 208 209 210 211 212 213 214 215 216 217 218 219 220 221 222 223 224 225 226 227 228 229 230 231 232 233 234 235 236 237 238 239 240 241 242 243 244 245 246 247 248 249 250 251 252 253 254 255 ) ;
	test ${?} -eq 0 ;
	assertTrue "The function responds to 255 arguments and choice 0 with status 0" ${?} ;

	rstring=$( echo 0 | solicit_choice 001 002 003 004 005 006 007 008 009 010 011 012 013 014 015 016 017 018 019 020 021 022 023 024 025 026 027 028 029 030 031 032 033 034 035 036 037 038 039 040 041 042 043 044 045 046 047 048 049 050 051 052 053 054 055 056 057 058 059 060 061 062 063 064 065 066 067 068 069 070 071 072 073 074 075 076 077 078 079 080 081 082 083 084 085 086 087 088 089 090 091 092 093 094 095 096 097 098 099 100 101 102 103 104 105 106 107 108 109 110 111 112 113 114 115 116 117 118 119 120 121 122 123 124 125 126 127 128 129 130 131 132 133 134 135 136 137 138 139 140 141 142 143 144 145 146 147 148 149 150 151 152 153 154 155 156 157 158 159 160 161 162 163 164 165 166 167 168 169 170 171 172 173 174 175 176 177 178 179 180 181 182 183 184 185 186 187 188 189 190 191 192 193 194 195 196 197 198 199 200 201 202 203 204 205 206 207 208 209 210 211 212 213 214 215 216 217 218 219 220 221 222 223 224 225 226 227 228 229 230 231 232 233 234 235 236 237 238 239 240 241 242 243 244 245 246 247 248 249 250 251 252 253 254 255 256 ) ;
	test ${?} -eq 255 -a \
	     "${rstring}" = "Function 'solicit_choice' requires from 2 to 255 arguments." ;
	assertTrue "The function responds to 256 argument with a message and status 255" ${?} ;

	#	I test how the function responds to various strings and valid choices.

	for tstring in "foo" 4 -1 \
	               "single'quote" 'double"quote' "nonツascii" "-leading hyphen" \
	               "white space" "white  spaces" "  leading spaces" "trailing spaces  " \
	               "tab	space" "tab		spaces" "		leading tabs" "trailing tabs		" \
	               "new
line" ;
	do

		for choice in 0 1 2 3 ;
		do

			rstring=$( echo ${choice} | solicit_choice "${tstring}" "option1" "option2" "option3" ) ;
			test ${?} -eq ${choice} -a "${rstring}" = "${tstring}

    1)   option1
    2)   option2
    3)   option3

Enter the number of an option to choose that option, or 0 to make no choice. " ;
			assertTrue "The function outputs the solicitation with message '${tstring}' and returns the choice '${choice}'" ${?}

			rstring=$( echo ${choice} | solicit_choice "message" "${tstring}" "option2" "option3" ) ;
			test ${?} -eq ${choice} -a "${rstring}" = "message

    1)   ${tstring}
    2)   option2
    3)   option3

Enter the number of an option to choose that option, or 0 to make no choice. " ;
			assertTrue "The function outputs the solicitation with first option '${tstring}' and returns the choice '${choice}'" ${?}

			rstring=$( echo ${choice} | solicit_choice "message" "option1" "${tstring}" "option3" ) ;
			test ${?} -eq ${choice} -a "${rstring}" = "message

    1)   option1
    2)   ${tstring}
    3)   option3

Enter the number of an option to choose that option, or 0 to make no choice. " ;
			assertTrue "The function outputs the solicitation with second option '${tstring}' and returns the choice '${choice}'" ${?}

			rstring=$( echo ${choice} | solicit_choice "message" "option1" "option2" "${tstring}" ) ;
			test ${?} -eq ${choice} -a "${rstring}" = "message

    1)   option1
    2)   option2
    3)   ${tstring}

Enter the number of an option to choose that option, or 0 to make no choice. " ;
			assertTrue "The function outputs the solicitation with third option '${tstring}' and returns the choice '${choice}'" ${?}

		done ;

	done ;

	#	I test how the function responds to various strings and invalid choices.

	for tstring in "foo" 4 -1 \
	               "single'quote" 'double"quote' "nonツascii" "-leading hyphen" \
	               "white space" "white  spaces" "  leading spaces" "trailing spaces  " \
	               "tab	space" "tab		spaces" "		leading tabs" "trailing tabs		" ;
	do

		for choice in 0 1 2 3 ;
		do

			rstring=$( ( echo "${tstring}" ; echo ${choice} ) |
			            solicit_choice "message" "option1" "option2" "option3" ) ;
			test ${?} -eq ${choice} -a "${rstring}" = "message

    1)   option1
    2)   option2
    3)   option3

Enter the number of an option to choose that option, or 0 to make no choice. Sorry, '${tstring}' is not a valid number.
message

    1)   option1
    2)   option2
    3)   option3

Enter the number of an option to choose that option, or 0 to make no choice. " ;
			assertTrue "The function rejects the choice '${tstring}' but returns the choice '${choice}'" ${?}

		done ;

	done ;

	unset tstring ;
	unset rstring ;
	unset choice ;

}
