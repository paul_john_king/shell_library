#	If the string «string» names an existing directory I can access and read
#	then the function call
#
#		list_directory_contents «string»
#
#	outputs a newline separated list of the non-hidden files in the directory
#	and returns 0, returns 1 if the string «string» does not name an existing
#	directory I can access and read, and prints a message and returns 2 if a
#	failure occurs.	 The behaviour of the function call is undefined if the
#	name of some non-hidden file in the directory contains newlines (though
#	«string» itself may contain newlines).

#	This library has no dependencies.

#	The posh Shell and Globbing
#	===========================

#	The posh shell does not expand path names while globbing.  For example,
#	given the directory tree
#
#	.
#	└── white space
#	    ├── file1
#	    ├── file2
#	    └── file3
#
#	at the current working directory, most shells produce a session like
#
#		$ ls white\ space
#		file1  file2  file3
#		$ ls "white space"
#		file1  file2  file3
#		$ ls 'white space'
#		file1  file2  file3$
#		echo white\ space/*
#		white space/file1 white space/file2 white space/file3
#		$ echo "white space"/*
#		white space/file1 white space/file2 white space/file3
#		$ echo 'white space'/*
#		white space/file1 white space/file2 white space/file3
#
#	but the posh shell produces a session like
#
#		$ ls white\ space
#		file1  file2  file3
#		$ ls "white space"
#		file1  file2  file3
#		$ ls 'white space'
#		file1  file2  file3
#		$ echo white\ space/*
#		white space/file1 white space/file2 white space/file3
#		$ echo "white space"/*
#		white space/*
#		$ echo 'white space'/*
#		white space/*
#
#	It seems that the posh shell interprets both quoted and unquoted strings as
#	arguments to the ls command, but does not expand file name patterns that
#	contain quoted components.  Therefore, in order to portably glob the
#	contents of a directory at path «path», execute
#
#		cd -- "«path»" ;
#
#		for file in ./* ;
#		do
#
#			...
#
#		done ;
#
#	Then each shell (including posh) properly expands «path» in the
#
#		cd -- "«path»"
#	
#	command, and no shell path expansion is necessary in the glob
#
#		for file in ./*

#	The zsh Shell and Hyphens
#	=========================

#	Some paths beginning with the hyphen character "-" can confuse the zsh
#	shell.  For example, the command
#
#		cd -- -1
#
#	elicits
#
#		cd: no such entry in dir stack
#
#	in zsh, even if directory '-1' exists.  To overcome this, prepend './' to
#	all relative paths that do not already have a './' prefix.

list_directory_contents()
{

	if test ${#} -lt 1 ;
	then

		#	I have less than 1 arguments.  I print a message and return 2.

		echo "Function 'list_directory_contents' requires at least 1 argument." ;

		return 2 ;

	fi ;

	#	In a subshell (in order to localise variables - see note "Global and
	#	Local Variables" in 'showcase.sh'),...

	(

		#	I prepend './' to my first argument if it does not already have a
		#	'./' prefix (see note "The zsh Shell and Hyphens" above).

		case "${1}" in

			/*|./*)

				#	My first argument is an absolute path or a relative with a
				#	'./' prefix.  I do nothing.

			;;

			*)

				#	My first argument is a relative without a './' prefix.  I
				#	prepend './' to it.

				set "./${1}" ;

			;;

		esac ;

		#	I try to change my working directory to the directory my first
		#	argument names (see note "The posh Shell and Globbing" above).

		if ! cd -- "${1}" 1>/dev/null 2>/dev/null ;
		then

			#	My first argument does not name an existing directory I can
			#	access.  I return 1.

			return 1 ;

		fi ;

		if ! test -r "." ;
		then

			#	My first argument does not name a directory I can read.  I
			#	return 1.

			return 1 ;

		fi ;

		for file in ./* ;
		do

			#	If the directory is empty then the glob returns "*".  In order
			#	to portably distinguish between an empty directory and a
			#	directory that contains just a file named "*", I test that each
			#	alleged file the glob returns is an actual file.

			if test -e "${file}" ;
			then

				#	The glob returned an actual file.  I print the name of the
				#	file with all leading directory components removed.

				echo "${file##*/}" ;

			fi ;

		#	If the directory is empty then the zsh shell returns a success
		#	status but prints a message to standard error.  I redirect this
		#	message to the null device.

		done 2>/dev/null ;

	) ;

}


