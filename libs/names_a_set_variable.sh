#	The function call
#
#		names_a_set_variable «string»
#
#	returns 0 if the string «string» is a valid variable name that names a set
#	variable, returns 1 if the string «string» is not a valid variable name
#	that names a set variable, and prints a message and returns 2 if a failure
#	occurs.

#	This library has the following dependencies:
#
#		is_a_valid_variable_name

names_a_set_variable()
{

	if test ${#} -lt 1 ;
	then

		#	I have less than 1 arguments.  I print a message and return 2.

		echo "Function 'names_a_set_variable' requires at least 1 argument." ;

		return 2 ;

	fi ;

	if ! is_a_valid_variable_name "${1}" ;
	then

		#	My first argument is not a valid variable name.  I return 1.

		return 1 ;

	fi ;

	#	If the string «string» names a set variable then shell parameter
	#	expansion subtitutes the string "x" for
	#
	#		"${«string»+x}"
	#
	#	but subtitutes the empty string "" otherwise.  Thus the command
	#
	#		test "${«string»+x}"
	#
	#	returns 0 if «string» names a set variable, non-0 otherwise.  Thus, if
	#	my first argument holds the string «string» then the evaluation
	#
	#		eval "test \"\${${1}+x}\" ;"
	#
	#	executes the command
	#
	#		test "${«string»+x}"
	#
	#	which returns 0 if «string» names a set variable, non-zero otherwise.

	if eval "test \"\${${1}+x}\" ;" ;
	then

		#	My first argument names a set variable.  I return 0.

		return 0 ;

	else

		#	My first argument does not name a set variable.  I return 1.

		return 1 ;

	fi ;

}
