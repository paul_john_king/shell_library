set -u ;

if ! test "${PROJECT_HOME+x}" ;
then

	echo "PROJECT_HOME is not set." >&2 ;
	exit 1 ;

fi ;

. "${PROJECT_HOME}/libs/list_directory_contents.sh" ;

#	One Time Set Up
#	===============

#	Constants
#	---------

#	This is a testbed directory in which I carry out all of my tests.

readonly TESTBED_NAME="testbed" ;
readonly TESTBED_PATH="${PROJECT_HOME}/${TESTBED_NAME}" ;

#	oneTimeSetUp
#	------------

oneTimeSetUp()
{

	if test -e "${TESTBED_PATH}" ;
	then

		#	The testbed directory already exists -- perhaps a previous test
		#	crashed without calling one time tear down -- so I call one time
		#	tear down here.

		oneTimeTearDown ;

	fi ;

	#	In a subshell,...

	(

		#	Testbed directory
		#	.................

		#	I try to create the testbed directory.

		if ! mkdir -p -- "${TESTBED_PATH}" ;
		then

			#	I failed.  I print an error message to standard error, tear
			#	down my test infrastructure, and exit with a failure status.

			echo "I cannot create the directory '${TESTBED_PATH}'." >&2 ;
			oneTimeTearDown ;
			exit 1 ;

		fi ;

		mkdir "${TESTBED_PATH}/foobar" ;

		#	Infrastructure to test how the function responds to non-directories
		#	...................................................................

		#	In the testbed directory, I try to create a plain file.

		if ! touch -- "${TESTBED_PATH}/plain-file" ;
		then

			#	I failed.  I print an error message to standard error, tear
			#	down my test infrastucture and exit with a failure status.

			echo "I cannot create the file '${TESTBED_PATH}/plain-file'." >&2 ;
			oneTimeTearDown ;
			exit 1 ;

		fi ;

		#	Infrastructure to test how the function responds to empty
		#	directories and globs
		#	.........................................................

		#	In the testbed directory, I try to create an empty directory.

		if ! mkdir -p -- "${TESTBED_PATH}/empty" ;
		then

			#	I failed.  I print an error message to standard error, tear
			#	down my test infrastucture and exit with a failure status.

			echo "I cannot create the directory '${TESTBED_PATH}/empty'." >&2 ;
			oneTimeTearDown ;
			exit 1 ;

		fi ;

		#	In the testbed directory, I try to create a directory containing
		#	exactly a file named "*".

		if ! mkdir -p -- "${TESTBED_PATH}/glob" ;
		then

			#	I failed.  I print an error message to standard error, tear
			#	down my test infrastucture and exit with a failure status.

			echo "I cannot create the directory '${TESTBED_PATH}/glob'." >&2 ;
			oneTimeTearDown ;
			exit 1 ;

		fi ;

		if ! touch -- "${TESTBED_PATH}/glob/*" ;
		then

			#	I failed.  I print an error message to standard error, tear
			#	down my test infrastucture and exit with a failure status.

			echo "I cannot create the file '${TESTBED_PATH}/glob/*'." >&2 ;
			oneTimeTearDown ;
			exit 1 ;

		fi ;

		#	Infrastructure to test how the function responds to hidden files
		#	................................................................

		#	In the testbed directory, I try to create a visible directory and a
		#	hidden directory, each containing exactly a visible file and a
		#	hidden file.

		for dname in ".hidden" "visible" ;
		do

			if ! mkdir -p -- "${TESTBED_PATH}/${dname}" ;
			then

				#	I failed.  I print an error message to standard error, tear
				#	down my test infrastucture and exit with a failure status.

				echo "I cannot create the directory '${TESTBED_PATH}/${dname}'." >&2 ;
				oneTimeTearDown ;
				exit 1 ;

			fi ;

			for fname in ".hidden" "visible" ;
			do

				if ! touch -- "${TESTBED_PATH}/${dname}/${fname}" ;
				then

					#	I failed.  I print an error message to standard error,
					#	tear down my test infrastucture and exit with a failure
					#	status.

					echo "I cannot create the file '${TESTBED_PATH}/${dname}/${fname}'." >&2 ;
					oneTimeTearDown ;
					exit 1 ;

				fi ;

			done ;

		done ;

		#	Infrastructure to test how the function responds to subdirectories
		#	..................................................................

		#	In the testbed directory, I try to create a directory containing
		#	exactly a file and a subdirectory.

		if ! mkdir -p -- "${TESTBED_PATH}/subdirectory" ;
		then

			#	I failed.  I print an error message to standard error, tear
			#	down my test infrastucture and exit with a failure status.

			echo "I cannot create the directory '${TESTBED_PATH}/subdirectory'." >&2 ;
			oneTimeTearDown ;
			exit 1 ;

		fi ;

		if ! mkdir -p -- "${TESTBED_PATH}/subdirectory/directory" ;
		then

			#	I failed.  I print an error message to standard error, tear
			#	down my test infrastucture and exit with a failure status.

			echo "I cannot create the directory '${TESTBED_PATH}/subdirectory/directory'." >&2 ;
			oneTimeTearDown ;
			exit 1 ;

		fi ;

		if ! touch -- "${TESTBED_PATH}/subdirectory/file" ;
		then

			#	I failed.  I print an error message to standard error, tear
			#	down my test infrastucture and exit with a failure status.

			echo "I cannot create the file '${TESTBED_PATH}/subdirectory/file'." >&2 ;
			oneTimeTearDown ;
			exit 1 ;

		fi ;

		#	Infrastructure to test how the function responds to symbolic links
		#	..................................................................

		#	In the testbed directory, I try to create a directory containing a
		#	file and a symbolic link to the file, and I try to create a
		#	symbolic link to the directory.

		if ! mkdir -p -- "${TESTBED_PATH}/target" ;
		then

			#	I failed.  I print an error message to standard error, tear
			#	down my test infrastucture and exit with a failure status.

			echo "I cannot create the directory '${TESTBED_PATH}/target'." >&2 ;
			oneTimeTearDown ;
			exit 1 ;

		fi ;

		if ! touch -- "${TESTBED_PATH}/target/target" ;
		then

			#	I failed.  I print an error message to standard error, tear
			#	down my test infrastucture and exit with a failure status.

			echo "I cannot create the file '${TESTBED_PATH}/target/target'." >&2 ;
			oneTimeTearDown ;
			exit 1 ;

		fi ;

		if ! ln -s "${TESTBED_PATH}/target/target" "${TESTBED_PATH}/target/symlink" ;
		then

			#	I failed.  I print an error message to standard error, tear
			#	down my test infrastucture and exit with a failure status.

			echo "I cannot create the symbolic link '${TESTBED_PATH}/target/symlink' to file '${TESTBED_PATH}/target/target'." >&2 ;
			oneTimeTearDown ;
			exit 1 ;

		fi ;

		if ! ln -s "${TESTBED_PATH}/target" "${TESTBED_PATH}/symlink" ;
		then

			#	I failed.  I print an error message to standard error, tear
			#	down my test infrastucture and exit with a failure status.

			echo "I cannot create the symbolic link '${TESTBED_PATH}/symlink' to file '${TESTBED_PATH}/target'." >&2 ;
			oneTimeTearDown ;
			exit 1 ;

		fi ;

		#	Infrastructure to test how the function responds to file names
		#	..............................................................

		#	In the testbed directory, I try to create directories with names
		#	that contain characters (such as non-ASCII characters, shell
		#	special characters, leading hyphens, single and double quotes,
		#	spaces, tabs and newlines) that might confuse a shell, and in each
		#	directory try to create files with names that contain characters
		#	(such as non-ASCII characters, shell special characters, leading
		#	hyphens, single and double quotes, spaces and tabs, but not
		#	newlines, since the behaviour of the 'list_directory_contents
		#	«path»' function call is undefined if a file in «path» contains a
		#	newline) that might confuse a shell.

		for dname in "4" "-1" \
		             "nonツascii" \
					 "shell !$&*@#" \
		             "-leading hyphen" \
		             "single'quote" 'double"quote' \
		             "white space" "white  spaces" "  leading spaces" "trailing spaces  " \
		             "tab	space" "tab		spaces" "		leading tabs" "trailing tabs		" \
		             "new
line" ;
		do

			if ! mkdir -p -- "${TESTBED_PATH}/${dname}" ;
			then

				#	I failed.  I print an error message to standard error, tear
				#	down my test infrastructure, and exit with a failure
				#	status.

				echo "I cannot create the directory '${TESTBED_PATH}/${dname}'." >&2 ;
				oneTimeTearDown ;
				exit 1 ;

			fi ;

			for fname in "4" "-1" \
		                 "nonツascii" \
					     "shell !$&*@#" \
		                 "-leading hyphen" \
		                 "single'quote" 'double"quote' \
		                 "white space" "white  spaces" "  leading spaces" "trailing spaces  " \
		                 "tab	space" "tab		spaces" "		leading tabs" "trailing tabs		" ;
			do

				if ! touch -- "${TESTBED_PATH}/${dname}/${fname}" ;
				then

					#	I failed.  I print an error message to standard error,
					#	tear down my test infrastucture and exit with a failure
					#	status.

					echo "I cannot create the file '${TESTBED_PATH}/${dname}/${fname}'." >&2 ;
					oneTimeTearDown ;
					exit 1 ;

				fi ;

			done ;

		done ;

		#	Infrastructure to test how the function responds to file
		#	permissions
		#	........................................................

		#	In the testbed directory, I try to create subdirectories with
		#	different permissions, each containing files with different
		#	permissions.

		for dname in "000" "100" "200" "300" "400" "500" "600" "700" ;
		do

			if ! mkdir -p -- "${TESTBED_PATH}/${dname}" ;
			then

				#	I failed.  I print an error message to standard error, tear
				#	down my test infrastucture and exit with a failure status.

				echo "I cannot create the directory '${TESTBED_PATH}/${dname}'." >&2 ;
				oneTimeTearDown ;
				exit 1 ;

			fi ;

			for fname in "000" "100" "200" "300" "400" "500" "600" "700" ;
			do

				if ! touch -- "${TESTBED_PATH}/${dname}/${fname}" ;
				then

					#	I failed.  I print an error message to standard error,
					#	tear down my test infrastucture and exit with a failure
					#	status.

					echo "I cannot create the file '${TESTBED_PATH}/${dname}/${fname}'." >&2 ;
					oneTimeTearDown ;
					exit 1 ;

				fi ;

				if ! chmod "${fname}" -- "${TESTBED_PATH}/${dname}/${fname}" ;
				then

					#	I failed.  I print an error message to standard error,
					#	tear down my test infrastucture and exit with a failure
					#	status.

					echo "I cannot set the permissions of directory '${TESTBED_PATH}/${dname}/${fname}' to '${fname}'." >&2 ;
					oneTimeTearDown ;
					exit 1 ;

				fi ;

			done ;

			if ! chmod "${dname}" -- "${TESTBED_PATH}/${dname}" ;
			then

				#	I failed.  I print an error message to standard error, tear
				#	down my test infrastucture and exit with a failure status.

				echo "I cannot set the permissions of directory '${TESTBED_PATH}/${dname}' to '${dname}'." >&2 ;
				oneTimeTearDown ;
				exit 1 ;

			fi ;

		done ;

	) ;

}

#	call_function
#	-------------

#	If
#
#		«tpath» is an absolute path to the testbed directory, and
#
#		«tname» is the last directory component of «tpath»,
#
#	then the function call
#
#		call_function «estring» «estatus» «dname_1» ... «dname_n»
#
#	asserts that
#
#		for each directory component «dname» in «dname_1», ..., «dname_n»,
#
#			for each path «piname» in '«dname»', './«dname»',
#			'../«tname>/«dname»', and '«testbed»/«dname»',
#
#				the function call
#
#					list_directory_contents «pname»
#
#				prints the expected string «estring» (after sorting) and
#				returns the expected status «estatus», and
#
#	prints a message and returns 2 if a failure occurs.

call_function()
{

	if test ${#} -lt 2 ;
	then

		#	I have less than 2 arguments.  I print a message and return 2.

		echo "Function 'call_function' requires at least 2 argument." ;

		return 2 ;

	fi ;

	#	I write the sorted expected string and the expected status to variables
	#	and shift them off of my parameter stack.

	estring=$( echo "${1}" | sort ) ;
	estatus=${2} ;

	shift 2 ;

	#	For each directory component,...

	for dname in "${@}" ;
	do

		#	For each path,...

		for pname in "${dname}" "./${dname}" "../${TESTBED_NAME}/${dname}" "${TESTBED_PATH}/${dname}" ;
		do

			#	I assert that the list_directory_contents function call prints
			#	the expected string (after sorting) and returns the expected
			#	status.

			#rstring=$( list_directory_contents "${pname}" | sort ) ;
			#test ${?} -eq ${estatus} -a "${rstring}" = "${estring}" ;
			#assertTrue "The function responds appropriately to the contents of directory '${pname}'" ${?}

			rstring=$( list_directory_contents "${pname}" ) ;
			rstatus=${?} ;
			rstring=$( echo "${rstring}" | sort ) ;
			test ${rstatus} -eq ${estatus} -a "${rstring}" = "${estring}" ;
			assertTrue "The function responds appropriately to the contents of directory '${pname}'" ${?}

		done ;

	done ;

	unset estring ;
	unset estatus ;
	unset dname ;
	unset pname ;
	unset rstring ;
	unset rstatus ;

}

#	Test Function
#	=============

test_list_directory_contents()
{

	#	In a subshell,...

	(

		#	I make the testbed directory my current working directory.

		cd "${TESTBED_PATH}" ;

		#	I test how the function responds to an incorrect number of arguments.

		rstring=$( list_directory_contents ) ;
		test ${?} -eq 2 -a \
		     "${rstring}" = "Function 'list_directory_contents' requires at least 1 argument." ;
		assertTrue "The function responds to 0 arguments with a message and status 2" ${?} ;

		#	I test how the function responds to non-existent directories,
		#	non-directories, empty directories and globs.
		
		call_function ""  1 "non-existent" ;
		call_function ""  1 "plain-file" ;
		call_function ""  0 "empty" ;
		call_function "*" 0 "glob" ;

		#	I test how the function responds to hidden files, subdirectories,
		#	and symbolic links.

		call_function "visible" 0 ".hidden" "visible" ;
		call_function "directory
file" 0 "subdirectory" ;
		call_function "target
symlink" 0 "target" "symlink" ;

		#	I test how the function responds to file names.

		call_function "4
-1
nonツascii
shell !$&*@#
-leading hyphen
single'quote
double\"quote
white space
white  spaces
  leading spaces
trailing spaces  
tab	space
tab		spaces
		leading tabs
trailing tabs		" \
0 \
"4" "-1" \
"nonツascii" \
"shell !$&*@#" \
"-leading hyphen" \
"single'quote" 'double"quote' \
"white space" "white  spaces" "  leading spaces" "trailing spaces  " \
"tab	space" "tab		spaces" "		leading tabs" "trailing tabs		" \
"new
line" ;

		#	I test how the function responds to directory permissions.

		call_function "000
100
200
300
400
500
600
700" 0 "500" "700" ;

		call_function "" 1 "000" "100" "200" "300" "400" "600" ;

	)

}

#	One Time Tear Down
#	==================

oneTimeTearDown()
{

	#	I try to give myself sufficient permissions to delete each file in the
	#	testbed directory.

	find "${TESTBED_PATH}" -type d -exec chmod 700 \{\} \;
	find "${TESTBED_PATH}" -type f -exec chmod 600 \{\} \;

	#	I try to delete the testbed directory.

	if ! rm -rf -- "${TESTBED_PATH}" ;
	then

		#	I failed.  I print an error message to standard error, and exit
		#	with a failure status.

		echo "I cannot delete the directory '${TESTBED_PATH}'." >&2 ;
		exit 1 ;

	fi ;

}
