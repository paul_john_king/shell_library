set -u ;

if ! test "${PROJECT_HOME+x}" ;
then

	echo "PROJECT_HOME is not set." >&2 ;
	exit 1 ;

fi ;

. "${PROJECT_HOME}/libs/is_a_natural_number.sh" ;
. "${PROJECT_HOME}/libs/print_choice.sh" ;

test_print_choice()
{

	#	I test how the function responds to an incorrect number of arguments.

	rstring=$( print_choice ) ;
	test ${?} -eq 2 -a \
	     "${rstring}" = "Function 'print_choice' requires at least 1 argument." ;
	assertTrue "The function responds to 0 arguments with a message and status 2" ${?} ;

	#	I test how the function responds to various indices and strings.

	for tstring in "-1" \
	               "nonツascii" \
	               "shell !$&*@#" \
	               "-leading hyphen" \
	               "single'quote" 'double"quote' \
	               "white space" "white  spaces" "  leading spaces" "trailing spaces  " \
	               "tab	space" "tab		spaces" "		leading tabs" "trailing tabs		" \
	               "new
line" ;
	do

		rstring=$( print_choice "${tstring}" "tstring" ) ;
		test ${?} -eq 1 -a "${rstring}" = "" ;
		assertTrue "The function responds to index '${tstring}' and string 'tstring' with status 1" ${?} ;

		rstring=$( print_choice 1 "${tstring}" ) ;
		test ${?} -eq 0 -a "${rstring}" = "${tstring}" ; 
		assertTrue "The function responds to index 1 and string '${tstring}' with string '${tstring}' and status 0" ${?} ;

	done ;

	for tstring in "1" "2" "3" "4" ;
	do

		rstring=$( print_choice "${tstring}" "1" "2" "3" "4" ) ;
		test ${?} -eq 0 -a "${rstring}" = "${tstring}" ; 
		assertTrue "The function responds to index ${tstring} and options '1', '2', '3' and '4' with string '${tstring}' and status 0" ${?} ;

	done ;

	for tstring in "-1" "0" "5" ;
	do

		rstring=$( print_choice "${tstring}" "1" "2" "3" "4" ) ;
		test ${?} -eq 1 -a "${rstring}" = "" ; 
		assertTrue "The function responds to index ${tstring} and options '1', '2', '3' and '4' with status 1" ${?} ;

	done ;

	unset tstring ;
	unset rstring ;

}
