set -u ;

if ! test "${PROJECT_HOME+x}" ;
then

	echo "PROJECT_HOME is not set." >&2 ;
	exit 1 ;

fi ;

. "${PROJECT_HOME}/libs/is_a_valid_variable_name.sh" ;
. "${PROJECT_HOME}/libs/names_a_set_variable.sh" ;

test_names_a_set_variable()
{

	#	I test how the function responds to an incorrect number of arguments.

	rstring=$( names_a_set_variable ) ;
	test ${?} -eq 2 -a \
	     "${rstring}" = "Function 'names_a_set_variable' requires at least 1 argument." ;
	assertTrue "The function responds to 0 arguments with a message and status 2" ${?} ;

	#	I test how the function responds to an invalid variable name.

	for tstring in "8foo" "8foo8" "8foo8bar" "8_foo" "8_foo8" "8_foo8bar" \
	               "8FOO" "8FOO8" "8FOO8BAR" "8_FOO" "8_FOO8" "8_FOO8BAR" \
	               "8FoO" "8fOo8" "8fOo8bAr" "8_fOo" "8_fOo8" "8_FoO8BaR" \
	               "" \
	               "nonツascii" \
	               "shell !$&*@#" \
	               "-leading hyphen" \
	               "single'quote" 'double"quote' \
	               "white space" "white  spaces" "  leading spaces" "trailing spaces  " \
	               "tab	space" "tab		spaces" "		leading tabs" "trailing tabs		" \
	               "new
line" ;
	do

		rstring=$( names_a_set_variable "${tstring}" ) ;
		test ${?} -eq 1 -a "${rstring}" = "" ;
		assertTrue "The function responds to invalid variable name '${tstring}' with no message and status 1" ${?} ;

	done ;

	#	I test how the function responds to the valid name of an unset variable.

	rstring=$( unset tstring ; names_a_set_variable tstring ) ;
	test ${?} -eq 1 -a "${rstring}" = "" ;
	assertTrue "The function responds to the valid name 'tstring' of an unset variable with no message and status 1" ${?} ;

	#	I test how the function responds to the valid name of a set but null
	#	variable.

	rstring=$( unset tstring ; tstring= ; names_a_set_variable tstring ) ;
	test ${?} -eq 0 -a "${rstring}" = "" ;
	assertTrue "The function responds to the valid name 'tstring' of a set but null variable with no message and status 0" ${?} ;

	#	I test how the function responds to the valid name of a set and
	#	non-null variable.

	rstring=$( unset string ; tstring="tstring" ; names_a_set_variable tstring ) ;
	test ${?} -eq 0 -a "${rstring}" = "" ;
	assertTrue "The function responds to the valid name 'tstring' of a set and non-null variable with no message and status 0" ${?} ;

	unset tstring ;
	unset rstring ;

}


