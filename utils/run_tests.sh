#!/bin/bash

#	Run many test scripts in many shells.

#	I treat each reference to an unset variable as an error, unset IFS to
#	ensure default word-splitting on space, tab and new line, and allow
#	patterns which match no files to expand to a null string.

set -u ;
unset IFS ;
shopt -s nullglob ;

#	I identify, fix, export and print the project home in bold blue to standard output.

if ! PROJECT_HOME=$( cd "${0%/*}/.." && pwd -P ) ;
then

	echo "I cannot identify the project home." ;
	exit 1 ;

fi ;

declare -rx PROJECT_HOME ;

echo -e "\e[01;34mThe project home is '${PROJECT_HOME}'.\e[00m" ;

#	I identify, fix and print the shunit2 executable in bold blue to standard
#	outpout.

if ! SHUNIT2=$( which "${PROJECT_HOME}/utils/shunit2_pjk.sh" ) ;
then

	echo "I cannot identify the shunit2 executable." ;
	exit 1 ;

fi ;

declare -r SHUNIT2 ;

echo -e "\e[01;34mThe shunit2 executable is '${SHUNIT2}'.\e[00m" ;

#	This constant holds an array of shells.

declare -ra SHELLS=( "bash" "busybox ash" "dash" "ksh93" "lksh" "mksh" "posh" "sh" "zsh" ) ;

#	I identify and fix the test scripts.

TEST_SCRIPTS=( "${PROJECT_HOME}/tests/"* ) ;

if test "${#TEST_SCRIPTS[@]}" -eq 0 ;
then

	echo "I cannot identify any test scripts." ;
	exit 1 ;

fi ;

declare -r TEST_SCRIPTS ;

#	These variables hold respectively the number of shells, the number of
#	missing shells, and the number of shells in which running the script
#	returns a failure status.

declare -i shell_count=0 ;
declare -i missing_shell_count=0 ;
declare -i failed_shell_count=0 ;

#	For each shell,...

for shell in "${SHELLS[@]}" ;
do

	#	I print the name of the shell in white on blue to standard output.
	
	echo -e "\e[00;38;44mShell: '${shell}'\e[00m" ;

	if ! ( which "${shell%% *}" ) 1>/dev/null 2>/dev/null ;
	then

		#	The shell does not exist.  I print a message in bold yellow to
		#	standard output, increment the number of missing shells, and
		#	continue to the next shell.

		echo -e "\e[01;33mI cannot find: '${shell}'\e[00m" ;

		(( missing_shell_count++ )) ;

		continue ;

	fi ;

	#	These variables hold respectively the number of test scripts, and the
	#	number of test scripts which report some failure in this shell.

	declare -i test_script_count=0 ;
	declare -i failed_test_script_count=0 ;

	#	For each test script,...

	for test_script in "${TEST_SCRIPTS[@]}" ;
	do

		#	I print the name of the test script in white on blue to standard
		#	output.
	
		echo -e "\e[00;38;44mTest script: '${test_script}'\e[00m" ;

		#	I run the test script in the shell and capture its return status.

		${shell} "${SHUNIT2}" "${test_script}" ;

		rstatus=${?} ;

		if test ${rstatus} -eq 0 ;
		then

			#	The test script returns the success status.  I print the return
			#	status in bold blue to standard output.

			echo -e "\e[01;34mExit status: ${rstatus}\e[00m" ;

		else

			#	The test script returns a failure status.  I print the return
			#	status in bold yellow to standard output, and increment the
			#	number of test scripts which report some failure in this shell.

			echo -e "\e[01;33mExit status: ${rstatus}\e[00m" ;

			(( failed_test_script_count++ )) ;

		fi ;

		#	I increment the number of test scripts.

		(( test_script_count++ )) ;

	done ;

	if test ${failed_test_script_count} -eq 0 ;
	then

		#	Each test script reports success in this shell.  I print this
		#	result in bold blue to standard output.

		echo -e "\e[01;34mEach test script reports success in the '${shell}' shell.\e[00m" ;

	else

		#	Some test script reports some failure in this shell.  I print this
		#	result in bold yellow to standard output, and increment the number
		#	of shells in which some test script reports some failure.

		echo -e "\e[01;33m${failed_test_script_count} of ${test_script_count} test scripts report some failure in the '${shell}' shell.\e[00m" ;

		(( failed_shell_count++ )) ;

	fi ;

	#	I increment the number of shells.

	(( shell_count++ )) ;

done ; 

if test ${missing_shell_count} -ne 0 ;
then

	#	I could not find some shells.  I print this result in bold yellow to
	#	standard output.

	echo -e "\e[01;33mI cannot find ${missing_shell_count} shells.\e[00m" ;

fi ;

if test ${failed_shell_count} -eq 0 ;
then

	#	Each test script reports success in each shell.  I print this result in
	#	bold blue to standard output.

	echo -e "\e[01;34mEach test script reports success in each found shell.\e[00m" ;

else

	#	Some test script reports some failure in some shell.  I print this
	#	result in bold yellow to standard output.

	echo -e "\e[01;33mSome test script reports some failure in ${failed_shell_count} of ${shell_count} found shells.\e[00m" ;

fi ;

#	I return the number of shells in which some test script reports some
#	failure.

exit ${failed_shell_count} ;
