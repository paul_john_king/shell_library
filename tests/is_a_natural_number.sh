set -u ;

if ! test "${PROJECT_HOME+x}" ;
then

	echo "PROJECT_HOME is not set." >&2 ;
	exit 1 ;

fi ;

. "${PROJECT_HOME}/libs/is_a_natural_number.sh" ;

test_is_a_natural_number()
{

	#	I test how the function responds to an incorrect number of arguments.

	rstring=$( is_a_natural_number ) ;
	test ${?} -eq 2 -a \
	     "${rstring}" = "Function 'is_a_natural_number' requires at least 1 argument." ;
	assertTrue "The function responds to 0 arguments with a message and status 2" ${?} ;

	#	I test how the function responds the non-representation of a natural
	#	number.

	for tstring in "a"  "a23"  "1a3"  "12a"  \
	               " "  " 23"  "1 3"  "12 "  \
	               "-"  "-23"  "1-3"  "12-"  \
	               "."  ".23"  "1.3"  "12."  \
	               "ツ" "ツ23" "1ツ3" "12ツ" \
	               "'"  "'23"  "1'3"  "12'"  \
	               '"'  '"23'  '1"3'  '12"'  \
	               "8foo" "8foo8" "8foo8bar" "8_foo" "8_foo8" "8_foo8bar" \
	               "8FOO" "8FOO8" "8FOO8BAR" "8_FOO" "8_FOO8" "8_FOO8BAR" \
	               "8FoO" "8fOo8" "8fOo8bAr" "8_fOo" "8_fOo8" "8_FoO8BaR" \
	               "" \
	               "nonツascii" \
	               "shell !$&*@#" \
	               "-leading hyphen" \
	               "single'quote" 'double"quote' \
	               "white space" "white  spaces" "  leading spaces" "trailing spaces  " \
	               "tab	space" "tab		spaces" "		leading tabs" "trailing tabs		" \
	               "new
line" ;
	do

		rstring=$( is_a_natural_number "${tstring}" ) ;
		test ${?} -eq 1 -a "${rstring}" = "" ;
		assertTrue "The function responds to the non-representation '${tstring}' of a natural number with no message and status 1" ${?} ;

	done ;

	#	I test how the function responds the representation of a natural
	#	number.

	for tstring in "0" "007" "90125" "12345678901234567890123456789012345678901234567890" ;
	do

		rstring=$( is_a_natural_number "${tstring}" ) ;
		test ${?} -eq 0 -a "${rstring}" = "" ;
		assertTrue "The function responds to the representation '${tstring}' of a natural number with no message and status 0" ${?} ;

	done ;

	unset tstring ;
	unset rstring ;

}
